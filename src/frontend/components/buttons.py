from typing import Dict, Literal
import streamlit as st
from lib.url import get_home_url


def back_to_home_button(
    params: Dict[str, str] = None,
    text: str = "🏠 Back to Home",
):
    background_color = (
        "#192D43" if st.session_state.theme["base"] == "dark" else "#E9F2FC"
    )
    text_color = "#E9F2FC" if st.session_state.theme["base"] == "dark" else "#004280"

    home_page_url = get_home_url(params)

    st.markdown(
        f'<a href="{home_page_url}" target="_self" style="color: {text_color}; text-decoration: none; background-color: {background_color}; padding: 0.5rem 1rem; border-radius: 0.5rem; display: inline-block;">{text}</a>',
        unsafe_allow_html=True,
    )


def link_button(
    text: str,
    url: str,
    icon: str = None,
    icon_position: Literal["left", "right"] = "left",
):
    icon_html = (
        f'<span style="margin-{"right" if icon_position == "left" else "left"}: 0.5rem; background-color: #787777; padding: 0.5rem 1rem;">{icon}</span>'
        if icon
        else ""
    )
    icon_content = (
        f"{icon_html}<span style='flex-grow: 1; margin-left: 0.5rem;'>{text}</span>"
        if icon_position == "left"
        else f"<span style='flex-grow: 1; margin-left: 1rem;'>{text}</span>{icon_html}"
    )

    st.markdown(
        f"""<a href="{url}" target="_self" style="color: white; text-decoration: none; background-color: rgba(49, 51, 63, 0.4); border-radius: 0.5rem; display: inline-block; width: 100%; text-align: left;">
            <div style="display: flex; align-items: center;">
                {icon_content}
            </div>
        </a>""",
        unsafe_allow_html=True,
    )
