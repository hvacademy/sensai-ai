# SensAI

SensAI is an open-source, AI-powered Learning Management System (LMS) which enables educators to create their own Coursera-style structured courses with the help of AI while serving as a coach that offers real-time feedback to learners, helping them improve through personalized coaching that encourages them to think for themselves instead of giving away the answers. Teachers get access to detailed learning data at the level of each learner along with AI-generated insights to better understand and support their learners.

Our mission is to make quality education accessible to everyone everywhere in the world.

If you are using SensAI and have any feedback for us or want any help with using SensAI, please consider [joining our community](https://chat.whatsapp.com/LmiulDbWpcXIgqNK6fZyxe) of AI + Education builders and reaching out to us.

If you want to contribute to SensAI, please look at the `Contributing` section below.

To get started with using SensAI, please refer to our [Documentation](https://docs.sensai.hyperverge.org) which explains all the key features of SensAI along with demo videos and a step-by-step guide for common use cases.

Our public roadmap is live [here](https://hyperverge.notion.site/fa1dd0cef7194fa9bf95c28820dca57f?v=ec52c6a716e94df180dcc8ced3d87610). Go check it out and let us know what you think we should build next!

## Contributing
To learn more about making a contribution to SensAI, please see our [Contributing guide](./docs/CONTRIBUTING.md).

## Installation
Refer to the [INSTALL.md](./docs/INSTALL.md) file for instructions on how to install and run the app locally.

## Deployment
Use the `Dockerfile` provided to build a docker image and deploy the image to whatever infra makes sense for you. We use an EC2 instance and you can refer to the `.gitlab-ci.yml` and `docker-compose.ai.demo.yml` files to understand how we do Continuous Deployment (CD).

## Community
We are building a community of creators, builders, teachers, learners, parents, entrepreneurs, non-profits and volunteers who are excited about the future of AI and education. If you identify as one and want to be part of it, consider [joining our community](https://chat.whatsapp.com/LmiulDbWpcXIgqNK6fZyxe).

Our thinking on how AI can impact Education is summarized in the mindmap below:
![ai + education thesis](./images/thesis.png)